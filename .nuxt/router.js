import Vue from 'vue'
import Router from 'vue-router'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _6886a86a = () => interopDefault(import('..\\pages\\scan.vue' /* webpackChunkName: "pages/scan" */))
const _a2be1bf2 = () => interopDefault(import('..\\pages\\hemato\\binocrit\\index.vue' /* webpackChunkName: "pages/hemato/binocrit/index" */))
const _307fe654 = () => interopDefault(import('..\\pages\\hemato\\rixathon\\index.vue' /* webpackChunkName: "pages/hemato/rixathon/index" */))
const _0e322618 = () => interopDefault(import('..\\pages\\hemato\\zarzio\\index.vue' /* webpackChunkName: "pages/hemato/zarzio/index" */))
const _36fd951b = () => interopDefault(import('..\\pages\\hemato\\ziextenzo\\index.vue' /* webpackChunkName: "pages/hemato/ziextenzo/index" */))
const _a9f28978 = () => interopDefault(import('..\\pages\\inmuno\\erelzi\\index.vue' /* webpackChunkName: "pages/inmuno/erelzi/index" */))
const _14fc54fd = () => interopDefault(import('..\\pages\\inmuno\\hyrimoz\\index.vue' /* webpackChunkName: "pages/inmuno/hyrimoz/index" */))
const _5b8dfacf = () => interopDefault(import('..\\pages\\inmuno\\zessly\\index.vue' /* webpackChunkName: "pages/inmuno/zessly/index" */))
const _28b0d8f1 = () => interopDefault(import('..\\pages\\hemato\\binocrit\\Desktop.vue' /* webpackChunkName: "pages/hemato/binocrit/Desktop" */))
const _1807bb46 = () => interopDefault(import('..\\pages\\hemato\\binocrit\\Mobile.vue' /* webpackChunkName: "pages/hemato/binocrit/Mobile" */))
const _d3170800 = () => interopDefault(import('..\\pages\\hemato\\rixathon\\Desktop.vue' /* webpackChunkName: "pages/hemato/rixathon/Desktop" */))
const _427f3d24 = () => interopDefault(import('..\\pages\\hemato\\rixathon\\Mobile.vue' /* webpackChunkName: "pages/hemato/rixathon/Mobile" */))
const _0d3866c4 = () => interopDefault(import('..\\pages\\hemato\\zarzio\\Desktop.vue' /* webpackChunkName: "pages/hemato/zarzio/Desktop" */))
const _1b14f5e0 = () => interopDefault(import('..\\pages\\hemato\\zarzio\\Mobile.vue' /* webpackChunkName: "pages/hemato/zarzio/Mobile" */))
const _0c710705 = () => interopDefault(import('..\\pages\\hemato\\ziextenzo\\Desktop.vue' /* webpackChunkName: "pages/hemato/ziextenzo/Desktop" */))
const _7733e1c9 = () => interopDefault(import('..\\pages\\hemato\\ziextenzo\\Mobile.vue' /* webpackChunkName: "pages/hemato/ziextenzo/Mobile" */))
const _ba6d7224 = () => interopDefault(import('..\\pages\\inmuno\\erelzi\\Desktop.vue' /* webpackChunkName: "pages/inmuno/erelzi/Desktop" */))
const _f760fe80 = () => interopDefault(import('..\\pages\\inmuno\\erelzi\\Mobile.vue' /* webpackChunkName: "pages/inmuno/erelzi/Mobile" */))
const _65bf5667 = () => interopDefault(import('..\\pages\\inmuno\\hyrimoz\\Desktop.vue' /* webpackChunkName: "pages/inmuno/hyrimoz/Desktop" */))
const _590d1e27 = () => interopDefault(import('..\\pages\\inmuno\\hyrimoz\\Mobile.vue' /* webpackChunkName: "pages/inmuno/hyrimoz/Mobile" */))
const _4e7ecfb9 = () => interopDefault(import('..\\pages\\inmuno\\zessly\\Desktop.vue' /* webpackChunkName: "pages/inmuno/zessly/Desktop" */))
const _369f9ad6 = () => interopDefault(import('..\\pages\\inmuno\\zessly\\Mobile.vue' /* webpackChunkName: "pages/inmuno/zessly/Mobile" */))
const _30cfb196 = () => interopDefault(import('..\\pages\\index.vue' /* webpackChunkName: "pages/index" */))

// TODO: remove in Nuxt 3
const emptyFn = () => {}
const originalPush = Router.prototype.push
Router.prototype.push = function push (location, onComplete = emptyFn, onAbort) {
  return originalPush.call(this, location, onComplete, onAbort)
}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: decodeURI('/realidad-aumentada/'),
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/scan",
    component: _6886a86a,
    name: "scan"
  }, {
    path: "/hemato/binocrit",
    component: _a2be1bf2,
    name: "hemato-binocrit"
  }, {
    path: "/hemato/rixathon",
    component: _307fe654,
    name: "hemato-rixathon"
  }, {
    path: "/hemato/zarzio",
    component: _0e322618,
    name: "hemato-zarzio"
  }, {
    path: "/hemato/ziextenzo",
    component: _36fd951b,
    name: "hemato-ziextenzo"
  }, {
    path: "/inmuno/erelzi",
    component: _a9f28978,
    name: "inmuno-erelzi"
  }, {
    path: "/inmuno/hyrimoz",
    component: _14fc54fd,
    name: "inmuno-hyrimoz"
  }, {
    path: "/inmuno/zessly",
    component: _5b8dfacf,
    name: "inmuno-zessly"
  }, {
    path: "/hemato/binocrit/Desktop",
    component: _28b0d8f1,
    name: "hemato-binocrit-Desktop"
  }, {
    path: "/hemato/binocrit/Mobile",
    component: _1807bb46,
    name: "hemato-binocrit-Mobile"
  }, {
    path: "/hemato/rixathon/Desktop",
    component: _d3170800,
    name: "hemato-rixathon-Desktop"
  }, {
    path: "/hemato/rixathon/Mobile",
    component: _427f3d24,
    name: "hemato-rixathon-Mobile"
  }, {
    path: "/hemato/zarzio/Desktop",
    component: _0d3866c4,
    name: "hemato-zarzio-Desktop"
  }, {
    path: "/hemato/zarzio/Mobile",
    component: _1b14f5e0,
    name: "hemato-zarzio-Mobile"
  }, {
    path: "/hemato/ziextenzo/Desktop",
    component: _0c710705,
    name: "hemato-ziextenzo-Desktop"
  }, {
    path: "/hemato/ziextenzo/Mobile",
    component: _7733e1c9,
    name: "hemato-ziextenzo-Mobile"
  }, {
    path: "/inmuno/erelzi/Desktop",
    component: _ba6d7224,
    name: "inmuno-erelzi-Desktop"
  }, {
    path: "/inmuno/erelzi/Mobile",
    component: _f760fe80,
    name: "inmuno-erelzi-Mobile"
  }, {
    path: "/inmuno/hyrimoz/Desktop",
    component: _65bf5667,
    name: "inmuno-hyrimoz-Desktop"
  }, {
    path: "/inmuno/hyrimoz/Mobile",
    component: _590d1e27,
    name: "inmuno-hyrimoz-Mobile"
  }, {
    path: "/inmuno/zessly/Desktop",
    component: _4e7ecfb9,
    name: "inmuno-zessly-Desktop"
  }, {
    path: "/inmuno/zessly/Mobile",
    component: _369f9ad6,
    name: "inmuno-zessly-Mobile"
  }, {
    path: "/",
    component: _30cfb196,
    name: "index"
  }],

  fallback: false
}

export function createRouter () {
  return new Router(routerOptions)
}
